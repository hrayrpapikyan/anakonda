#!/usr/bin/python 
# Enabling debug mode
import cgitb
cgitb.enable()
# Starting app
from Core.App import App
from Core.Config import Config
config = Config.get()
app = App(config)
app.start()


