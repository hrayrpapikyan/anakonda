from Core.Router import Router


class App:
    """
    Web Application Main Class
    """

    config = {}

    def __init__(self, config):
        self.config = config
        self.router = Router(self.config)

    def start(self):
        """
        Init Web Application
        """
        self.send_headers()
        self.process_request()

    def process_request(self):
        """
        Creates Controller class and calls appropriate method
        """
        class_obj = self.dynamic_import('Controllers.' + self.router.controller.title(), self.router.controller.title())
        getattr(class_obj, self.router.action)()

    @staticmethod
    def dynamic_import(module, class_name):
        """
        Import and instantiate Controller object
        :param module:
        :param class_name:
        :return: (className) object of Controller class
        """
        mod = __import__(module, fromlist=[class_name])
        class_obj = getattr(mod, class_name)()
        return class_obj

    @staticmethod
    def send_headers():
        """
        Set Application Headers
        :return:
        """
        print("Content-Type: text/html")
        print()
