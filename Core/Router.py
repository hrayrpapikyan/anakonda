import os


class Router:

    def __init__(self, config):
        self.requestUri = os.environ['REQUEST_URI']
        self.defaultController = config['default']['controller']
        self.controller = None
        self.action = 'index'
        self.arguments = {}
        self.set_params()

    def set_params(self):
        request_uri_stripped = self.requestUri.strip('/')

        if request_uri_stripped == '':
            self.controller = self.defaultController
        else:
            request_uri_array = request_uri_stripped.split('/')
            request_params_length = len(request_uri_array)
            self.controller = request_uri_array[0]

            if request_params_length > 1:
                self.action = request_uri_array[1]

            if request_params_length > 2:
                arguments_part = request_uri_array[2:]
                self.arguments = {arguments_part[k]: arguments_part[k + 1] for k in range(0, len(arguments_part) - 1) if k% 2 == 0}



