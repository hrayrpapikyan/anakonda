import json
import os


class Config:
    """
    Configuration core class
    """

    def __init__(self):
        pass

    @staticmethod
    def get():
        """
        loads json file with respective environment (dev or prod)
        :return: configuration list
        """
        conf_path = "Config/config.json"
        json_data = open(conf_path)
        data = json.load(json_data)
        return data[os.environ['ENV']]

