import urllib.request
import re
from Core.Config import Config


class Templating:
    """
    Templating System
    """

    def __init__(self, path, data, layout):
        self.data = data
        self.path = path
        self.file = ''
        self.layout = layout

    def load_content(self):
        """
        loads the html file
        using urllib module here, and not just using open() function for
        the html file, because that way messes up the encoding
        """
        config = Config.get()
        self.data['base_url'] = config['routing']['base_url']
        file = urllib.request.urlopen(config['routing']['base_url'] + 'Views/' + self.path + '.html').read().decode("utf8")
        self.file = file

    def find_loop(self):
        """
        finds one highest level loop
        :return: returns it's position
        in case if there are no loops return False
        """
        all_loop_starts = [m.start() for m in re.finditer('\{\{for', self.file)]
        all_loop_ends = [m.end() for m in re.finditer('endfor\}\}', self.file)]
        all_loop_starts.append(all_loop_ends[len(all_loop_ends) - 1] + 1)
        # zero_level_loops = []
        i = 0
        length = len(all_loop_starts)
        while i < length - 1:
            for j in range(i + 1, length):
                if all_loop_starts[j] > all_loop_ends[i]:
                    return {'start': all_loop_starts[i], 'end': all_loop_ends[j-1]}
            # if len(zero_level_loops) > 0:
            #     break
            i += 1

        return False

    def replace_loop(self, start_end):
        """
        replaces loop by multiplying it's content
        :param start_end:
        """
        content = self.file[start_end['start']:start_end['end']]
        content = content[5:-10]
        m = re.search("\}\}", content)
        first_pos_closing_double_quotes = m.start()
        before_first_closing_double_quotes = content[:first_pos_closing_double_quotes]
        content = content[first_pos_closing_double_quotes + 2:]
        splitted = before_first_closing_double_quotes.split(' in ')
        loop_variable_name = splitted[1].strip()
        item_variable_name = splitted[0].strip()
        variable_array = loop_variable_name.split('.')
        variable = self.data[variable_array[0]]
        for i in range(1, len(variable_array)):
            array_key = self.convert_to_int_if_possible(variable_array[i])
            if self.data_exists(variable, array_key) == False:
                self.file = self.file[:start_end['start']] + self.file[start_end['end']:]
                return False
            variable = variable[array_key]
        self.data[loop_variable_name] = variable
        if isinstance(self.data[loop_variable_name], list):
            self.data[loop_variable_name] = {i: self.data[loop_variable_name][i] for i in range(0, len(self.data[loop_variable_name]))}
        all_item_variable_name_starts = [m.start() for m in re.finditer(item_variable_name, content)]
        item_variable_name_length = len(item_variable_name)
        temp_content = ''
        for key in self.data[loop_variable_name]:
            start_pos = 0
            key_str = str(key)
            key_length = len(key_str)
            temp_content_in_loop = ''
            for item_variable_name_start in all_item_variable_name_starts:
                item_variable_name_end = item_variable_name_start + item_variable_name_length
                if content[item_variable_name_end] != ' ' and content[item_variable_name_end] != '.' and content[item_variable_name_end] != '}':
                    continue
                temp_content_in_loop += content[start_pos:item_variable_name_start] + loop_variable_name + '.' + key_str
                start_pos = item_variable_name_end - 1 + key_length
            temp_content_in_loop += content[start_pos:]
            temp_content += temp_content_in_loop
        self.file = self.file[:start_end['start']] + temp_content + self.file[start_end['end']:]
        return True

    def data_exists(self, arr, key):
        try:
            arr[key]
        except KeyError:
            return False
        return True

    def convert_to_int_if_possible(self, string):
        try:
            int(string)
        except ValueError:
            return string
        return int(string)

    def find_keywords_and_variables(self, string):
        m = re.search("\{\{", string)
        start = m.end()
        m = re.search("\}\}", string)
        end = m.start()
        content = string[start:end]
        return content


    def find_and_replace_for_loops(self):
        loop_start_and_end_position = self.find_loop()
        self.replace_loop(loop_start_and_end_position)

    def find_and_replace_if_statements(self):
        if_start_and_end_position = self.find_if()
        self.replace_if(if_start_and_end_position)

    def replace_if(self, start_end):
        """
        replaces loop by multiplying it's content
        :param start_end:
        """
        content = self.file[start_end['start']:start_end['end']]
        content = content[4:-9]
        first_closing_braces = re.search("\}\}", content)
        variable_name = content[:first_closing_braces.start()].strip()
        variable_array = variable_name.split('.')
        variable = self.data[variable_array[0]]
        for i in range(1, len(variable_array)):
            array_key = self.convert_to_int_if_possible(variable_array[i])
            if self.data_exists(variable, array_key) == False:
                self.file = self.file[:start_end['start']] + self.file[start_end['end']:]
                return False
            variable = variable[array_key]
        self.data[variable_name] = variable
        if '{{else}}' in content:
            else_pos = re.search("\{\{else\}\}", content)
            else_pos_start = else_pos.start()
            else_pos_end = else_pos.end()
            if self.data[variable_name]:
                temp_content = content[first_closing_braces.end():else_pos_start]
            else:
                temp_content = content[else_pos_end:]
        else:
            if self.data[variable_name]:
                temp_content = content[first_closing_braces.end():]
            else:
                temp_content = ''
        self.file = self.file[:start_end['start']] + temp_content + self.file[start_end['end']:]
        return True


    def find_if(self):
        all_if_starts = [m.start() for m in re.finditer('\{\{if', self.file)]
        all_if_ends = [m.end() for m in re.finditer('\{\{endif\}\}', self.file)]
        return self.get_maximum_close_positions(all_if_starts, all_if_ends)

    def get_maximum_close_positions(self, starts, ends):
        length = len(starts)
        minimum_positive_difference_keys = {'start': starts.index(min(starts)), 'end': ends.index(max(ends))}
        minimum_positive_difference = max(ends) - min(starts)
        for i in range(0, length):
            for j in range(0, length):
                if minimum_positive_difference >= ends[j] - starts[i] > 0:
                    minimum_positive_difference = ends[j] - starts[i]
                    minimum_positive_difference_keys = {'start': starts[i], 'end': ends[j]}
        return minimum_positive_difference_keys

    def find_variables(self):
        variable_name_start = re.search("\{\{", self.file).end()
        variable_name_end = re.search("\}\}", self.file).start()
        variable_name = self.file[variable_name_start:variable_name_end].strip()
        variable_array = variable_name.split('.')
        variable = self.data[variable_array[0]]
        for i in range(1, len(variable_array)):
            array_key = self.convert_to_int_if_possible(variable_array[i])
            if self.data_exists(variable, array_key) == False:
                self.file = self.file[:variable_name_start - 2] + self.file[variable_name_end + 2:]
                return False
            variable = variable[array_key]
        self.data[variable_name] = variable
        self.file = self.file[:variable_name_start - 2] + str(self.data[variable_name]) +self.file[variable_name_end + 2:]

    def render(self):
        """
        renders the view file
        """
        self.load_content()
        while '{{for' in self.file:
            self.find_and_replace_for_loops()
        while '{{if' in self.file:
            self.find_and_replace_if_statements()
        while '{{' in self.file:
            self.find_variables()
        self.path = 'Layout/' + self.layout
        self.data['view_content'] = self.file
        self.file = ''
        self.load_content()
        while '{{for' in self.file:
            self.find_and_replace_for_loops()
        while '{{if' in self.file:
            self.find_and_replace_if_statements()
        while '{{' in self.file:
            self.find_variables()
        print(self.file)