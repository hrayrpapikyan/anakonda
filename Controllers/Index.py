from Controllers.Base import Base


class Index(Base):

    def __init__(self):
        Base.__init__(self)

    def index(self):
        posts = [
            {
                "author": "author1",
                "title": "title1",
                "subtitle": "subtitle1",
                "link": "link1"
            },
            {
                "author": "author2",
                "title": "title2",
                "subtitle": "subtitle2",
                "link": "link2"
            }
        ]
        data = {
            "posts": posts,
        }
        Base.render(self, 'Index/index', data)

