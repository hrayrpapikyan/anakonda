from Services.Templating import Templating


class Base:

    def __init__(self):
        self.layout = 'main'

    def set_layout(self, layout):
        self.layout = layout

    def render(self, path, data= {}):
        self.templating = Templating(path, data, self.layout)
        self.templating.render()

